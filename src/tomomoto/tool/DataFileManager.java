package tomomoto.tool;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import tomomoto.DataSnapMemory;
import tomomoto.judge.TreeJudge;
import tree.GrowTree;

public class DataFileManager {
	
	private static int modelNum = 0;
	private static final String DATA_FOLDER = "O:\\workspace_eclipse\\AutoMarioSimpleBestTreeVer3\\data10";
	public void countup(){
		modelNum++;
	}
	
	public void writeRecord(int maxStage,int maxCurrentPage,int maxCurrentPagePosiX){
		try{
			File file = new File(DATA_FOLDER+"\\ModelRecord.csv");
			FileWriter filewriter = new FileWriter(file, true);
		    filewriter.write(modelNum+","+maxStage+","+maxCurrentPage+","+maxCurrentPagePosiX+"\n");
		    filewriter.close();
		    
		}catch(IOException e){
			System.out.println(e);
		}
	}
	public void writeTreeJudge(TreeJudge treeJudge){
		try{
			File file = new File(DATA_FOLDER+"\\TreeModelFirst"+modelNum+".csv");
		    FileWriter filewriter = new FileWriter(file, true);
		    treeJudge.getFirstTree().write(filewriter);
		    filewriter.close();
		    
		    file = new File(DATA_FOLDER+"\\TreeModelSecond"+modelNum+".csv");
		    filewriter = new FileWriter(file, true);
		    treeJudge.getSecondTree().write(filewriter);
		    filewriter.close();
		    
		}catch(IOException e){
			System.out.println(e);
		}
	}
	
	public TreeJudge readTreeJudge(int modelNum){
		TreeJudge treeJudge = new TreeJudge();
		try{
			File file = new File(DATA_FOLDER+"\\TreeModelFirst"+modelNum+".csv");
		    FileWriter filewriter = new FileWriter(file, true);
		    BufferedReader br = new BufferedReader(new FileReader(file));
			StringBuilder stringBuilder = new StringBuilder();
		    String dataLine = br.readLine();
			while(dataLine != null){
				stringBuilder.append(dataLine);
				dataLine = br.readLine();
			}
			GrowTree firstTree = new GrowTree();
			firstTree = firstTree.read(stringBuilder.toString());
			treeJudge.setFirstTree(firstTree);
			filewriter.close();
		}catch(IOException e){
			System.out.println(e);
		}
		
		try{
			File file = new File(DATA_FOLDER+"\\TreeModelSecond"+modelNum+".csv");
		    FileWriter filewriter = new FileWriter(file, true);
		    BufferedReader br = new BufferedReader(new FileReader(file));
			StringBuilder stringBuilder = new StringBuilder();
		    String dataLine = br.readLine();
			while(dataLine != null){
				stringBuilder.append(dataLine);
				dataLine = br.readLine();
			}
			GrowTree secondTree = new GrowTree();
			secondTree = secondTree.read(stringBuilder.toString());
			treeJudge.setSecondTree(secondTree);
			filewriter.close();
		}catch(IOException e){
			System.out.println(e);
		}
		return treeJudge;
	}
	public void writeData(List<DataSnapMemory> studyMemoryList){
		try{
			File file = new File(DATA_FOLDER+"\\marioData.csv");
		    FileWriter filewriter = new FileWriter(file, true);

		    for(int i=0;i<studyMemoryList.size()-1;i++){
		    	DataSnapMemory dataSnapMemory = studyMemoryList.get(i);
		    	dataSnapMemory.write(filewriter);
		    }
		    filewriter.close();
		}catch(IOException e){
			System.out.println(e);
		}
	}
	
	public void read(List<DataSnapMemory> studyMemoryList){
		try{
			File file = new File("O:\\workspace_eclipse\\AutoMarioGrowTreeVer3\\data\\marioData.csv");
			BufferedReader br = new BufferedReader(new FileReader(file));
			
			String dataLine = br.readLine();
			while(dataLine != null){
				DataSnapMemory dataSnapMemory= new DataSnapMemory();
				dataSnapMemory.read(dataLine);
				studyMemoryList.add(dataSnapMemory);
				dataLine = br.readLine();
			}
			
		    br.close();
		}catch(IOException e){
			System.out.println(e);
		}
	}
	
}
