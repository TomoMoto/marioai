package tomomoto;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import tomomoto.judge.Judge;
import tomomoto.judge.TreeJudge;
import tomomoto.tool.DataFileManager;
import jp.tanakh.bjne.nes.Nes;
import jp.tanakh.bjne.nes.Renderer.InputInfo;

public class MarioController {
	private MarioDataReader marioDataReader = new MarioDataReader();
	private DataFileManager dataFileManager = new DataFileManager();
	private Random random = new Random();
	
	private TreeJudge judge = new TreeJudge();
	private List<DataMarioAction> menuActionList = new ArrayList<DataMarioAction>();
	private List<DataSnapMemory> memoryList = new ArrayList<DataSnapMemory>();
	private List<DataSnapMemory> bestMemoryList = new ArrayList<DataSnapMemory>();
	private List<DataSnapMemory> goodMemoryList = new ArrayList<DataSnapMemory>();
	private List<DataSnapMemory> deadMemoryList = new ArrayList<DataSnapMemory>();

	private int maxStage =-1;
	private int maxCurrentPage = -1;
	private int maxCurrentPagePosiX = -1;
	private boolean gameOverCheck =true;
	
	static final int[][] keyDef = {
		{ KeyEvent.VK_Z, KeyEvent.VK_X, KeyEvent.VK_SHIFT,
				KeyEvent.VK_ENTER, KeyEvent.VK_UP, KeyEvent.VK_DOWN,
				KeyEvent.VK_LEFT, KeyEvent.VK_RIGHT, },
		{ KeyEvent.VK_V, KeyEvent.VK_B, KeyEvent.VK_N, KeyEvent.VK_M,
				KeyEvent.VK_O, KeyEvent.VK_COMMA, KeyEvent.VK_K,
				KeyEvent.VK_L, } };

	private InputInfo onKey(InputInfo inpi, int keyCode, boolean press) {
		for (int i = 0; i < 2; i++)
			for (int j = 0; j < 8; j++)
				if (keyCode == keyDef[i][j])
					inpi.buf[i * 8 + j] = (press ? 1 : 0);
		return inpi;
	}
	
	public MarioController(){
		//judge = dataFileManager.readTreeJudge(153);
	}
	
	public void action(InputInfo inpi, Nes nes){
		DataSnapMemory snapMemoryData = marioDataReader.readMemory(nes);

		
		if(snapMemoryData.getGameMode() == 1 && snapMemoryData.getPlayerX()==0 && snapMemoryData.getPlayerY()==-16){
			if(menuActionList.size()!=0){
				menuActionList = new ArrayList<DataMarioAction>();
			}
		}else if(snapMemoryData.getPlayerX() >=0 && snapMemoryData.getPlayerY() >=0 && judgeAction(snapMemoryData)){
			//新しくアクションを行ったとき
			
			//Score計算
			if(memoryList.size()>0){
				memoryList.get(memoryList.size()-1).calculateActionScore(snapMemoryData);
			}
			
			//進んでいない場合のペナルティ計算
			if(memoryList.size()>20){
				if(memoryList.get(memoryList.size()-20).addPlaceCheck(snapMemoryData)){
					snapMemoryData.addPlacePena();
					for(int i=1;i<40+1;i++){
						int tmpMemoryNo = memoryList.size()-i;
						if(0<tmpMemoryNo && tmpMemoryNo<=memoryList.size()-1){
							memoryList.get(tmpMemoryNo).addPlacePena();
						}
					}
				}
			}
			
			memoryList.add(snapMemoryData);
			
			//死んだときの判定
			if(memoryList.size()>=2 && memoryList.get(memoryList.size()-2).getMarioDieFlg()==0 && memoryList.get(memoryList.size()-1).getMarioDieFlg()==1){
				//死んだとき
				dataFileManager.writeRecord(snapMemoryData.getStageLevel(), snapMemoryData.getCurrentPage(), snapMemoryData.getCurrentPagePosiX());
				dataFileManager.writeData(goodMemoryList);
				dataFileManager.countup();
				
				//最高記録到達判定
				if( (snapMemoryData.getStageLevel() > maxStage)
						|| (snapMemoryData.getStageLevel() == maxStage && maxCurrentPage<snapMemoryData.getCurrentPage())
						|| (snapMemoryData.getStageLevel() == maxStage && maxCurrentPage==snapMemoryData.getCurrentPage() && maxCurrentPagePosiX < snapMemoryData.getCurrentPagePosiX())){
					
					//最高記録更新時
					maxStage = snapMemoryData.getStageLevel();
					maxCurrentPage = snapMemoryData.getCurrentPage();
					maxCurrentPagePosiX = snapMemoryData.getCurrentPagePosiX();
					
					//よかったデータを初期化
					if(gameOverCheck){
						gameOverCheck = false;
						bestMemoryList = new ArrayList<DataSnapMemory>();
					}
					for(int i=0;i<memoryList.size()-20;i++){
						if(memoryList.get(i).getActionScore()>=0){
							bestMemoryList.add(memoryList.get(i));
						}
					}

					//model保存
					dataFileManager.writeTreeJudge(judge);
				}else{
					//最高記録非更新時
					judge = new TreeJudge();
				}

				//よかったデータを収集
				for(int i=0;i<memoryList.size()-20;i++){
					if(memoryList.get(i).getActionScore()>=0){
						goodMemoryList.add(memoryList.get(i));
					}
				}
				while(goodMemoryList.size()>10000){
					goodMemoryList.remove(0);
				}
				//BestMemory追加
				if(gameOverCheck){
					goodMemoryList.addAll(bestMemoryList);
				}
				
				//DeadPena
				deadMemoryList = new ArrayList<DataSnapMemory>();
				for(int i=0;i<random.nextInt(40)+1;i++){
					if(memoryList.size()-1-i>=0){
						memoryList.get(memoryList.size()-1-i).addActionScore4Dead();
						deadMemoryList.add(memoryList.get(memoryList.size()-1-i));
					}
				}
				for(int i=0;i<memoryList.size()-20;i++){
					if(memoryList.get(i).getActionScore()<0){
						deadMemoryList.add(memoryList.get(i));
					}
				}
				while(deadMemoryList.size()>10000){
					deadMemoryList.remove(0);
				}
				
				judge.study(deadMemoryList,goodMemoryList);
				memoryList = new ArrayList<DataSnapMemory>();
			}
		};
		doAction(inpi);
	}
	
	private boolean judgeAction(DataSnapMemory snapMemoryData){
		if(snapMemoryData.getGameMode() == 0){
			//Title
			return setStartGameAction(snapMemoryData);
		}else if(snapMemoryData.getGameMode() == 3){
			//GameOver
			gameOverCheck = true;
			return setNoAction(snapMemoryData);
		}else{
			return judge.judge(snapMemoryData,menuActionList);
		}
	}
	
	private void doAction(InputInfo inpi){
		if(menuActionList.size()>0){
			onKey(inpi, KeyEvent.VK_X, menuActionList.get(0).isDashFlg());
			onKey(inpi, KeyEvent.VK_Z, menuActionList.get(0).isJumpFlg());
			onKey(inpi, KeyEvent.VK_LEFT, menuActionList.get(0).isLeftFlg());
			onKey(inpi, KeyEvent.VK_RIGHT, menuActionList.get(0).isRightFlg());
			onKey(inpi, KeyEvent.VK_DOWN, menuActionList.get(0).isDownFlg());
			onKey(inpi, KeyEvent.VK_ENTER, menuActionList.get(0).isStartFlg());
			if(menuActionList.get(0).isEnd()){
				menuActionList.remove(0);
			}
		}
	}
	
	private boolean setNoAction(DataSnapMemory snapMemoryData){
		snapMemoryData.setActionCode(DataSnapMemory.NO_ACTION);
		DataMarioAction marioAction = new DataMarioAction();
		marioAction.setTime(1);
		menuActionList.add(marioAction);
		
		return false;
	}
	
	private boolean setStartGameAction(DataSnapMemory snapMemoryData){
		snapMemoryData.setActionCode(DataSnapMemory.START_ACTION);
		
		DataMarioAction marioAction1 = new DataMarioAction();
		menuActionList.add(marioAction1);
		
		DataMarioAction marioAction2 = new DataMarioAction();
		marioAction2.setStartFlg(true);
		marioAction2.setTime(1);
		menuActionList.add(marioAction2);
		
		DataMarioAction marioAction3 = new DataMarioAction();
		menuActionList.add(marioAction3);
		
		return false;
	}
	
}