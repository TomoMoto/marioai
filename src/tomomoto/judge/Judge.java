package tomomoto.judge;

import java.util.List;

import tomomoto.DataMarioAction;
import tomomoto.DataSnapMemory;

public abstract class Judge {
	
	abstract public Judge clone();
	abstract public boolean judge(DataSnapMemory snapMemoryData, List<DataMarioAction> actionList);
	abstract public void study(List<DataSnapMemory> deadMemoryList,List<DataSnapMemory> goodMemoryList);
	
	public void setNoAction(DataSnapMemory snapMemoryData,List<DataMarioAction> actionList){
		snapMemoryData.setActionCode(DataSnapMemory.NO_ACTION);
		
		DataMarioAction marioAction = new DataMarioAction();
		marioAction.setTime(1);
		actionList.add(marioAction);
	}
	
	public void setRightAction(DataSnapMemory snapMemoryData,List<DataMarioAction> actionList){
		snapMemoryData.setActionCode(DataSnapMemory.RIGHT_ACTION);
		
		DataMarioAction marioAction = new DataMarioAction();
		marioAction.setRightFlg(true);
		marioAction.setDashFlg(true);
		marioAction.setTime(6);
		actionList.add(marioAction);
	}
	
	public void setLeftAction(DataSnapMemory snapMemoryData,List<DataMarioAction> actionList){
		snapMemoryData.setActionCode(DataSnapMemory.LEFT_ACTION);
		
		DataMarioAction marioAction = new DataMarioAction();
		marioAction.setLeftFlg(true);
		marioAction.setDashFlg(true);
		marioAction.setTime(6);
		actionList.add(marioAction);
	}

	public void setRightJumpAction(DataSnapMemory snapMemoryData,List<DataMarioAction> actionList){
		snapMemoryData.setActionCode(DataSnapMemory.RIGH_JUMP_ACTION);
		
		DataMarioAction marioAction = new DataMarioAction();
		marioAction.setRightFlg(true);
		marioAction.setDashFlg(true);
		marioAction.setJumpFlg(false);
		marioAction.setTime(2);
		actionList.add(marioAction);
		
		DataMarioAction marioAction2 = new DataMarioAction();
		marioAction2.setRightFlg(true);
		marioAction2.setDashFlg(true);
		marioAction2.setJumpFlg(true);
		marioAction2.setTime(22);
		actionList.add(marioAction2);
	}
	
	public void setJumpAction(DataSnapMemory snapMemoryData,List<DataMarioAction> actionList){
		snapMemoryData.setActionCode(DataSnapMemory.JUMP_ACTION);
		
		DataMarioAction marioAction = new DataMarioAction();
		marioAction.setRightFlg(true);
		marioAction.setJumpFlg(true);
		marioAction.setTime(8);
		actionList.add(marioAction);

		DataMarioAction marioAction2 = new DataMarioAction();
		marioAction2.setTime(1);
		actionList.add(marioAction2);
	}

}
