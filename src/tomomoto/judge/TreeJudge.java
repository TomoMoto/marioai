package tomomoto.judge;

import java.util.ArrayList;
import java.util.List;

import tomomoto.DataMarioAction;
import tomomoto.DataSnapMemory;
import tree.GrowTree;
import tree.TreeAnswer;
import tree.TreeData;

public class TreeJudge extends Judge{

	public static final int PARA_LENGTH = 10;

	private GrowTree firstTree = new GrowTree();
	private GrowTree secondTree = new GrowTree();
	
	public TreeJudge(){
	}
	
	public TreeJudge clone(){
		TreeJudge clone = new TreeJudge();
		clone.firstTree = firstTree.clone();
		clone.secondTree = secondTree.clone();
		
		return clone;
	}
	
	@Override
	public boolean judge(DataSnapMemory snapMemoryData,List<DataMarioAction> actionList) {
		if(actionList.size() == 0){
			TreeData treeData = new TreeData();
			treeData.setParaDataSet(getMarioAroundData(snapMemoryData));
			treeData.setAnswerFlg(snapMemoryData.getStudyFlg());
			
			TreeAnswer treeScore = firstTree.apply(treeData);
			if(treeScore.getTrueCnt() < treeScore.getFalseCnt()){
				setRightJumpAction(snapMemoryData,actionList);
			}else{
				TreeAnswer secondTreeScore = secondTree.apply(treeData);
				if(secondTreeScore.getTrueCnt() <= secondTreeScore.getFalseCnt()){
					setRightAction(snapMemoryData,actionList);
				}else{
					setLeftAction(snapMemoryData, actionList);
				}
			}
			
			return true;
		}else{
			return false;
		}
	}
	
	public void study(List<DataSnapMemory> deadMemoryList, List<DataSnapMemory> goodMemoryList){
		List<TreeData> treeDeadStudyList = new ArrayList<TreeData>();
		List<TreeData> secondDeadTreeStudyList = new ArrayList<TreeData>();
		
		for (int i = 0; i < deadMemoryList.size(); i++) {
			DataSnapMemory snapMemoryData = deadMemoryList.get(i);

			TreeData treeData = new TreeData();
			TreeData secondTreeData = new TreeData();
			
			treeData.setParaDataSet(getMarioAroundData(snapMemoryData));
			secondTreeData.setParaDataSet(getMarioAroundData(snapMemoryData));
			
			switch (snapMemoryData.getActionCode()) {
			case DataSnapMemory.RIGH_JUMP_ACTION:
				treeData.setAnswerFlg(true);
				treeDeadStudyList.add(treeData);
				break;
				
			case DataSnapMemory.RIGHT_ACTION:
				treeData.setAnswerFlg(false);
				treeDeadStudyList.add(treeData);
				
				secondTreeData.setAnswerFlg(true);
				secondDeadTreeStudyList.add(secondTreeData);
				break;
				
			case DataSnapMemory.LEFT_ACTION:
				treeData.setAnswerFlg(false);
				treeDeadStudyList.add(treeData);
					
				secondTreeData.setAnswerFlg(false);
				secondDeadTreeStudyList.add(secondTreeData);
				break;
			}
		}
		
		
		List<TreeData> treeGoodStudyList = new ArrayList<TreeData>();
		List<TreeData> secondTreeGoodStudyList = new ArrayList<TreeData>();
		for (int i = 0; i < goodMemoryList.size(); i++) {
			DataSnapMemory snapMemoryData = goodMemoryList.get(i);
			
			TreeData treeData = new TreeData();
			TreeData secondTreeData = new TreeData();
			treeData.setParaDataSet(getMarioAroundData(snapMemoryData));
			secondTreeData.setParaDataSet(getMarioAroundData(snapMemoryData));

			switch (snapMemoryData.getActionCode()) {
			case DataSnapMemory.RIGH_JUMP_ACTION:
				treeData.setAnswerFlg(false);
				treeGoodStudyList.add(treeData);
				break;
				
			case DataSnapMemory.RIGHT_ACTION:
				treeData.setAnswerFlg(true);
				treeGoodStudyList.add(treeData);
				
				secondTreeData.setAnswerFlg(false);
				secondTreeGoodStudyList.add(secondTreeData);
				break;
			
			case DataSnapMemory.LEFT_ACTION:
				treeData.setAnswerFlg(true);
				treeGoodStudyList.add(treeData);
				
				secondTreeData.setAnswerFlg(true);
				secondTreeGoodStudyList.add(secondTreeData);
				break;
			}
		}
		
		//System.out.println("firstTree");
		firstTree.study(treeDeadStudyList,treeGoodStudyList);
		//firstTree.printModel();
		
		//System.out.println("");
		//System.out.println("secondTree");
		secondTree.study(secondDeadTreeStudyList,secondTreeGoodStudyList);
		//secondTree.printModel();
	}
	
	
	public static double[] getMarioAroundData(DataSnapMemory snapMemoryData){
		double[] marioAroundData = new double[17];

		//Right RightBlock
		marioAroundData[0] = 0d;
		for(int i=0;i<PARA_LENGTH;i++){
			int x = (snapMemoryData.getPlayerX()/16+1+i) % (DataSnapMemory.TILE_PAGE_WIDTH_NUM*2);
			if(snapMemoryData.getCurrentPage()%2==1){
				x = (x + DataSnapMemory.TILE_PAGE_WIDTH_NUM) % (DataSnapMemory.TILE_PAGE_WIDTH_NUM*2);
			}
			int y = snapMemoryData.getPlayerY()/16;
			if(y>=0 && y<DataSnapMemory.TILE_HEIGHT_NUM && snapMemoryData.getPageBlock()[x][y]){
				marioAroundData[0] = (16d*16d-1) - ((i+1)*16d-(16-snapMemoryData.getPlayerX()%16));
				break;
			}
		}
		
		//Right UpBlock
		marioAroundData[1] = 0d;
		for(int j=0;j<2;j++){
			for(int i=0;i<PARA_LENGTH;i++){
				int x = (snapMemoryData.getPlayerX()/16+1+i+(DataSnapMemory.TILE_PAGE_WIDTH_NUM*2))% (DataSnapMemory.TILE_PAGE_WIDTH_NUM*2);
				if(snapMemoryData.getCurrentPage()%2==1){
					x = (x + DataSnapMemory.TILE_PAGE_WIDTH_NUM) % (DataSnapMemory.TILE_PAGE_WIDTH_NUM*2);
				}
				int y = snapMemoryData.getPlayerY()/16+1+j;
				if(y>=0 && y<DataSnapMemory.TILE_HEIGHT_NUM && snapMemoryData.getPageBlock()[x][y]){
					if(marioAroundData[1] < (double)(PARA_LENGTH-i)/(double)PARA_LENGTH){
						marioAroundData[1] = (16d*16d-1) - ((i+1)*16d-(16-snapMemoryData.getPlayerX()%16));
					}
					break;
				}
			}
		}
		
		marioAroundData[2] = 0d;
		for(int j=0;j<3;j++){
			for(int i=0;i<PARA_LENGTH;i++){
				int x = (snapMemoryData.getPlayerX()+1+i+(DataSnapMemory.TILE_PAGE_WIDTH_NUM*2))% (DataSnapMemory.TILE_PAGE_WIDTH_NUM*2);
				if(snapMemoryData.getCurrentPage()%2==1){
					x = (x + DataSnapMemory.TILE_PAGE_WIDTH_NUM) % (DataSnapMemory.TILE_PAGE_WIDTH_NUM*2);
				}
				int y = snapMemoryData.getPlayerY()+3+j;
				if(y>=0 && y<DataSnapMemory.TILE_HEIGHT_NUM && snapMemoryData.getPageBlock()[x][y]){
					if(marioAroundData[2] < (double)(PARA_LENGTH-i)/(double)PARA_LENGTH){
						marioAroundData[2] = (16d*16d-1) - ((i+1)*16d-(16-snapMemoryData.getPlayerX()%16));
					}
					break;
				}
			}
		}
		
		
		//Right DownBlock
		marioAroundData[3] = 0d;
		for(int j=0;j<2;j++){
			for(int i=0;i<PARA_LENGTH;i++){
				int x = (snapMemoryData.getPlayerX()+1+i+(DataSnapMemory.TILE_PAGE_WIDTH_NUM*2))% (DataSnapMemory.TILE_PAGE_WIDTH_NUM*2);
				if(snapMemoryData.getCurrentPage()%2==1){
					x = (x + DataSnapMemory.TILE_PAGE_WIDTH_NUM) % (DataSnapMemory.TILE_PAGE_WIDTH_NUM*2);
				}
				int y = snapMemoryData.getPlayerY()-1-j;
				if(y>=0 && y<DataSnapMemory.TILE_HEIGHT_NUM && snapMemoryData.getPageBlock()[x][y]){
					if(marioAroundData[3] < (double)(PARA_LENGTH-i)/(double)PARA_LENGTH){
						marioAroundData[3] = (16d*16d-1) - ((i+1)*16d-(16-snapMemoryData.getPlayerX()%16));
					}
					break;
				}
			}
		}
		
		marioAroundData[4] = 0d;
		for(int j=0;j<3;j++){
			for(int i=0;i<PARA_LENGTH;i++){
				int x = (snapMemoryData.getPlayerX()+1+i+(DataSnapMemory.TILE_PAGE_WIDTH_NUM*2))% (DataSnapMemory.TILE_PAGE_WIDTH_NUM*2);
				if(snapMemoryData.getCurrentPage()%2==1){
					x = (x + DataSnapMemory.TILE_PAGE_WIDTH_NUM) % (DataSnapMemory.TILE_PAGE_WIDTH_NUM*2);
				}
				int y = snapMemoryData.getPlayerY()-3-j;
				if(y>=0 && y<DataSnapMemory.TILE_HEIGHT_NUM && snapMemoryData.getPageBlock()[x][y]){
					if(marioAroundData[4] < (double)(PARA_LENGTH-i)/(double)PARA_LENGTH){
						marioAroundData[4] = (16d*16d-1) - ((i+1)*16d-(16-snapMemoryData.getPlayerX()%16));
					}
					break;
				}
			}
		}
		
		
		//Upside Block
		marioAroundData[5] = 0d;
		for(int i=0;i<PARA_LENGTH;i++){
			int x = (snapMemoryData.getPlayerX() +(DataSnapMemory.TILE_PAGE_WIDTH_NUM*2))% (DataSnapMemory.TILE_PAGE_WIDTH_NUM*2);
			if(snapMemoryData.getCurrentPage()%2==1){
				x = (x + DataSnapMemory.TILE_PAGE_WIDTH_NUM) % (DataSnapMemory.TILE_PAGE_WIDTH_NUM*2);
			}
			int y = snapMemoryData.getPlayerY()-1-i +DataSnapMemory.TILE_HEIGHT_NUM;
			if(y>=0 && y<DataSnapMemory.TILE_HEIGHT_NUM && snapMemoryData.getPageBlock()[x][y]){
				marioAroundData[5] = (double)(PARA_LENGTH-i)/(double)PARA_LENGTH *(16d*16d-1);
				break;
			}
		}
		
		marioAroundData[6] = 0d;
		for(int j=0;j<2;j++){
			for(int i=0;i<PARA_LENGTH;i++){
				int x = (snapMemoryData.getPlayerX() +1 +j +(DataSnapMemory.TILE_PAGE_WIDTH_NUM*2))% (DataSnapMemory.TILE_PAGE_WIDTH_NUM*2);
				if(snapMemoryData.getCurrentPage()%2==1){
					x = (x + DataSnapMemory.TILE_PAGE_WIDTH_NUM) % (DataSnapMemory.TILE_PAGE_WIDTH_NUM*2);
				}
				int y = snapMemoryData.getPlayerY()-1-i +DataSnapMemory.TILE_HEIGHT_NUM;
				if(y>=0 && y<DataSnapMemory.TILE_HEIGHT_NUM && snapMemoryData.getPageBlock()[x][y]){
					if(marioAroundData[6] < (double)(PARA_LENGTH-i)/(double)PARA_LENGTH){
						marioAroundData[6] = (double)(PARA_LENGTH-i)/(double)PARA_LENGTH *(16d*16d-1);
					}
					break;
				}
			}
		}
		

		marioAroundData[7] = 0d;
		for(int j=0;j<3;j++){
			for(int i=0;i<PARA_LENGTH;i++){
				int x = (snapMemoryData.getPlayerX() +3 +j +(DataSnapMemory.TILE_PAGE_WIDTH_NUM*2))% (DataSnapMemory.TILE_PAGE_WIDTH_NUM*2);
				if(snapMemoryData.getCurrentPage()%2==1){
					x = (x + DataSnapMemory.TILE_PAGE_WIDTH_NUM) % (DataSnapMemory.TILE_PAGE_WIDTH_NUM*2);
				}
				int y = snapMemoryData.getPlayerY()-1-i +DataSnapMemory.TILE_HEIGHT_NUM;
				if(y>=0 && y<DataSnapMemory.TILE_HEIGHT_NUM && snapMemoryData.getPageBlock()[x][y]){
					if(marioAroundData[7] < (double)(PARA_LENGTH-i)/(double)PARA_LENGTH){
						marioAroundData[7] = (double)(PARA_LENGTH-i)/(double)PARA_LENGTH *(16d*16d-1);
					}
					break;
				}
			}
		}

		
		//Downside Block
		marioAroundData[8] = 0d;
		double noBlockLength = 0d;
		for(int i=0;i<PARA_LENGTH;i++){
			int x = (snapMemoryData.getPlayerX()+1 +(DataSnapMemory.TILE_PAGE_WIDTH_NUM*2))% (DataSnapMemory.TILE_PAGE_WIDTH_NUM*2);
			if(snapMemoryData.getCurrentPage()%2==1){
				x = (x + DataSnapMemory.TILE_PAGE_WIDTH_NUM) % (DataSnapMemory.TILE_PAGE_WIDTH_NUM*2);
			}
			int y = (snapMemoryData.getPlayerY()+1+i +DataSnapMemory.TILE_HEIGHT_NUM) % DataSnapMemory.TILE_HEIGHT_NUM;
			if(y>=0 && y<DataSnapMemory.TILE_HEIGHT_NUM && snapMemoryData.getPageBlock()[x][y]){
				break;
			}else{
				noBlockLength++;
			}
		}
		marioAroundData[8] = (double)noBlockLength/(double)PARA_LENGTH *(16d*16d-1);
		
		
		for(int j=0;j<2;j++){
			marioAroundData[9] = 0d;
			noBlockLength = 0d;
			for(int i=0;i<PARA_LENGTH;i++){
				int x = (snapMemoryData.getPlayerX()+2+j +(DataSnapMemory.TILE_PAGE_WIDTH_NUM*2))% (DataSnapMemory.TILE_PAGE_WIDTH_NUM*2);
				if(snapMemoryData.getCurrentPage()%2==1){
					x = (x + DataSnapMemory.TILE_PAGE_WIDTH_NUM) % (DataSnapMemory.TILE_PAGE_WIDTH_NUM*2);
				}
				int y = (snapMemoryData.getPlayerY()+1+i +DataSnapMemory.TILE_HEIGHT_NUM) % DataSnapMemory.TILE_HEIGHT_NUM;
				if(y>=0 && y<DataSnapMemory.TILE_HEIGHT_NUM && snapMemoryData.getPageBlock()[x][y]){
					break;
				}else{
					noBlockLength++;
				}
			}
			if(marioAroundData[9] < (double)noBlockLength/(double)PARA_LENGTH){
				marioAroundData[9] = (double)noBlockLength/(double)PARA_LENGTH *(16d*16d-1);
			}
		}
		
		
		
		for(int j=0;j<3;j++){
			marioAroundData[9] = 0d;
			noBlockLength = 0d;
			for(int i=0;i<PARA_LENGTH;i++){
				int x = (snapMemoryData.getPlayerX()+4+j +(DataSnapMemory.TILE_PAGE_WIDTH_NUM*2))% (DataSnapMemory.TILE_PAGE_WIDTH_NUM*2);
				if(snapMemoryData.getCurrentPage()%2==1){
					x = (x + DataSnapMemory.TILE_PAGE_WIDTH_NUM) % (DataSnapMemory.TILE_PAGE_WIDTH_NUM*2);
				}
				int y = (snapMemoryData.getPlayerY()+1+i +DataSnapMemory.TILE_HEIGHT_NUM) % DataSnapMemory.TILE_HEIGHT_NUM;
				if(y>=0 && y<DataSnapMemory.TILE_HEIGHT_NUM && snapMemoryData.getPageBlock()[x][y]){
					break;
				}else{
					noBlockLength++;
				}
			}
			if(marioAroundData[9] < (double)noBlockLength/(double)PARA_LENGTH){
				marioAroundData[9] = (double)noBlockLength/(double)PARA_LENGTH *(16d*16d-1);
			}
		}
		
		
		//Enemy
		Double[] nearEnemyDistance = new Double[3];
		Integer[] nearEnemyX = new Integer[3];
		Integer[] nearEnemyY = new Integer[3];
		for(int i=0;i<3;i++){
			nearEnemyX[i]=1000;
			nearEnemyY[i]=1000;
			nearEnemyDistance[i]=null;
			
			marioAroundData[10+i] = 1000;
			marioAroundData[11+i] = 1000;
		}
		
		for(int i=0;i<5;i++){
			Integer enemyX = snapMemoryData.getEnemyX()[i];
			Integer enemyY = snapMemoryData.getEnemyY()[i];
			int playerX = snapMemoryData.getPlayerX();
			int playerY = snapMemoryData.getPlayerY();
			
			if(enemyX !=  null && enemyY !=  null){
				
				if(snapMemoryData.getCurrentPage()%2==1){
					if(enemyX>=DataSnapMemory.TILE_PAGE_WIDTH_NUM){
						enemyX = enemyX - DataSnapMemory.TILE_PAGE_WIDTH_NUM;
					}else{
						enemyX = enemyX + DataSnapMemory.TILE_PAGE_WIDTH_NUM;
					}
					if(playerX>=DataSnapMemory.TILE_PAGE_WIDTH_NUM){
						playerX = playerX - DataSnapMemory.TILE_PAGE_WIDTH_NUM;
					}else{
						playerX = playerX + DataSnapMemory.TILE_PAGE_WIDTH_NUM;
					}
				}
				
				int distanceX = enemyX - playerX;
				int distanceY = enemyY - playerY;
				
				double distance = Math.sqrt((distanceX)^2+(distanceY)^2);
				if(nearEnemyDistance[0] == null || nearEnemyDistance[0]>distance){
					//0->1
					nearEnemyDistance[1] = nearEnemyDistance[0];
					nearEnemyX[1] = nearEnemyX[0];
					nearEnemyY[1] = nearEnemyY[0];
					//1->2
					nearEnemyDistance[2] = nearEnemyDistance[1];
					nearEnemyX[2] = nearEnemyX[1];
					nearEnemyY[2] = nearEnemyY[1];
					
					nearEnemyDistance[0] = distance;
					nearEnemyX[0] = distanceX;
					nearEnemyY[0] = distanceY;
				}else if(nearEnemyDistance[1] == null || nearEnemyDistance[1]>distance){
					//1->2
					nearEnemyDistance[2] = nearEnemyDistance[1];
					nearEnemyX[2] = nearEnemyX[1];
					nearEnemyY[2] = nearEnemyY[1];
					
					nearEnemyDistance[1] = distance;
					nearEnemyX[1] = distanceX;
					nearEnemyY[1] = distanceY;
				}else if(nearEnemyDistance[2] == null || nearEnemyDistance[2]>distance){
					nearEnemyDistance[2] = distance;
					nearEnemyX[2] = distanceX;
					nearEnemyY[2] = distanceY;
				}
			}
		}
		
		for(int i=0;i<3;i++){
			marioAroundData[10+2*i] = nearEnemyX[i];
			marioAroundData[11+2*i] = nearEnemyY[i];
		}
		
		//MarioXSpeed
		int xSpeed = snapMemoryData.getMarioXSpeed();
		if(xSpeed>=128){
			xSpeed = -(255-xSpeed); 
		}
		marioAroundData[16] = xSpeed;
		
		return marioAroundData;
	}

	public GrowTree getFirstTree() {
		return firstTree;
	}
	public void setFirstTree(GrowTree firstTree) {
		this.firstTree = firstTree;
	}
	public GrowTree getSecondTree() {
		return secondTree;
	}
	public void setSecondTree(GrowTree secondTree) {
		this.secondTree = secondTree;
	}

	
}
