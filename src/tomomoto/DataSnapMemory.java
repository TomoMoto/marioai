package tomomoto;

import java.io.FileWriter;
import java.io.IOException;

public class DataSnapMemory{
	
	public static final int SCREEN_HEIGHT = MarioDataReader.SCREEN_HEIGHT;
	public static final int TILE_PAGE_WIDTH_NUM = MarioDataReader.TILE_WIDTH_NUM_PAGE;
	public static final int TILE_HEIGHT_NUM = (SCREEN_HEIGHT/16)-2;
	
	private int actionCode = NO_ACTION;
	private double actionScore = 0;
	
	private int gameMode;
	private int currentPage;
	private int currentPagePosiX;
	private int stageWorld;
	private int stageLevel;
	private int detailPlayerX;
	private int detailPlayerXPage;
	
	private int playerX;
	private int playerY;
	private int marioState;
	private int marioDieFlg;
	private int marioScore;
	private int marioXSpeed;
	
	private Integer[] enemyX = new Integer[5];
	private Integer[] enemyY = new Integer[5];
	private Boolean[][] pageBlock = new Boolean[TILE_PAGE_WIDTH_NUM*2][TILE_HEIGHT_NUM];

	
	public static final int START_ACTION = 0;
	public static final int NO_ACTION = 1;
	public static final int RIGHT_ACTION = 2;
	public static final int LEFT_ACTION = 3;
	public static final int RIGH_JUMP_ACTION = 4;
	public static final int JUMP_ACTION = 5;
	
	
	public int getGameMode() {
		return gameMode;
	}
	public void setGameMode(int gameMode) {
		this.gameMode = gameMode;
	}
	
	public int getStageWorld() {
		return stageWorld;
	}
	public void setStageWorld(int stageWorld) {
		this.stageWorld = stageWorld;
	}
	public int getStageLevel() {
		return stageLevel;
	}
	public void setStageLevel(int stageLevel) {
		this.stageLevel = stageLevel;
	}
	public int getMarioState() {
		return marioState;
	}
	public void setMarioState(int marioState) {
		this.marioState = marioState;
	}
	
	public int getMarioDieFlg() {
		return marioDieFlg;
	}
	public void setMarioDieFlg(int marioDieFlg) {
		this.marioDieFlg = marioDieFlg;
	}
	public int getMarioScore() {
		return marioScore;
	}
	public void setMarioScore(int marioScore) {
		this.marioScore = marioScore;
	}
	public int getMarioXSpeed() {
		return marioXSpeed;
	}
	public void setMarioXSpeed(int marioXSpeed) {
		this.marioXSpeed = marioXSpeed;
	}
	public Boolean[][] getPageBlock() {
		return pageBlock;
	}
	public void setPageBlock(Boolean[][] pageBlock) {
		this.pageBlock = pageBlock;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public int getCurrentPagePosiX() {
		return currentPagePosiX;
	}
	public void setCurrentPagePosiX(int currentPagePosiX) {
		this.currentPagePosiX = currentPagePosiX;
	}
	public int getPlayerX() {
		return playerX;
	}
	public void setPlayerX(int playerX) {
		this.playerX = playerX;
	}
	public int getPlayerY() {
		return playerY;
	}
	public void setPlayerY(int playerY) {
		this.playerY = playerY;
	}
	public Integer[] getEnemyX() {
		return enemyX;
	}
	public void setEnemyX(Integer[] enemyX) {
		this.enemyX = enemyX;
	}
	public Integer[] getEnemyY() {
		return enemyY;
	}
	public void setEnemyY(Integer[] enemyY) {
		this.enemyY = enemyY;
	}
	public int getActionCode() {
		return actionCode;
	}
	public void setActionCode(int actionCode) {
		this.actionCode = actionCode;
	}
	public double getActionScore() {
		return actionScore;
	}
	public void setActionScore(double actionScore) {
		this.actionScore = actionScore;
	}
	public int getDetailPlayerX() {
		return detailPlayerX;
	}
	public void setDetailPlayerX(int detailPlayerX) {
		this.detailPlayerX = detailPlayerX;
	}
	public int getDetailPlayerXPage() {
		return detailPlayerXPage;
	}
	public void setDetailPlayerXPage(int detailPlayerXPage) {
		this.detailPlayerXPage = detailPlayerXPage;
	}

	private static final double ACTION_SCORE_DEAD = -5000;
	private static final double ACTION_SCORE_MARIO_POWERUP = 500;
	
	public void calculateActionScore(DataSnapMemory afterSnapMemoryData){
		double actionScore = 0d;
		
		//MarioState
		double tmpMarioState = (double)(afterSnapMemoryData.getMarioState() - marioState);
		actionScore = actionScore + tmpMarioState*ACTION_SCORE_MARIO_POWERUP;
		
		//Score
		actionScore = actionScore + (double)(afterSnapMemoryData.getMarioScore() - marioScore);
		
		this.actionScore = actionScore;
	}
	
	public void addActionScore4Dead(){
		actionScore = actionScore + ACTION_SCORE_DEAD;
	}
	
	public void addPlacePena(){
		actionScore = actionScore - 100d;
	}
	
	public boolean addPlaceCheck(DataSnapMemory afterSnapMemoryData){
		double seedPlace = (afterSnapMemoryData.getCurrentPagePosiX()-getCurrentPagePosiX()) 
				+255 * (afterSnapMemoryData.getCurrentPage()-getCurrentPage());

		//System.out.println("seed"+seedPlace);
		/*
		if(afterSnapMemoryData.getCurrentPage()==getCurrentPage() && afterSnapMemoryData.getCurrentPagePosiX()==getCurrentPagePosiX()){
			actionScore = actionScore - 100d;
			return true;
		}else if(afterSnapMemoryData.getCurrentPage()==getCurrentPage() && afterSnapMemoryData.getCurrentPagePosiX()>=getCurrentPagePosiX()+20){
			actionScore = actionScore - 100d;
			return true;
		}
		*/
		if(seedPlace <20){
			actionScore = actionScore - 100d;
			return true;
		}
			
		return false;
	}
	
	public double getStudyScore() {
		if(actionScore>500){
			return 1d;
		}else if(actionScore>0){
			return 0.8d;
		}else if(actionScore==0){
			return 0.5d;
		}else if(actionScore<-500){
			return 0d;
		}else{
			return 0.2d;
		}
	}
	
	public boolean getStudyFlg() {
		if(actionScore>=0){
			return true;
		}else{
			return false;
		}
	}

	
	public void printAllMap(){
		System.out.println("ActionCode"+actionCode);
		System.out.println("ActionScore"+actionScore);
		
		for(int i=0;i<TILE_HEIGHT_NUM;i++){
			for(int j=0;j<TILE_PAGE_WIDTH_NUM*2;j++){
				printMap1Tile(j, i);
			}
			System.out.println();
		}
		System.out.println();
	}
	
	
	private static final int AROUND_MARIO_X = 12;
	public void printMarioAroundMap(){
		int startX = (playerX - 2 +TILE_PAGE_WIDTH_NUM*2);
		
		for(int i=1;i<TILE_HEIGHT_NUM-1;i++){
			for(int j=0;j<AROUND_MARIO_X;j++){
				printMap1Tile(((startX+j) % (TILE_PAGE_WIDTH_NUM*2)), i);
			}
			System.out.println();
		}
		System.out.println();
	}
	
	private void printMap1Tile(int x,int y){
		if(x == playerX && y==playerY){
			System.out.print("7");
		}else if(getEnemyFlg(x, y)){
			System.out.print("4");
		}else if(pageBlock[x][y]){
			System.out.print("1");
		}else{
			System.out.print(" ");
		}
	}
	
	public boolean getEnemyFlg(int x,int y){
		if((enemyX[0]!=null && enemyY[0]!=null) && x==enemyX[0] && y==enemyY[0]){
			return true;
		}else if((enemyX[1]!=null && enemyY[1]!=null) && x==enemyX[1] && y==enemyY[1]){
			return true;
		}else if((enemyX[2]!=null && enemyY[2]!=null) && x==enemyX[2] && y==enemyY[2]){
			return true;
		}else if((enemyX[3]!=null && enemyY[3]!=null) && x==enemyX[3] && y==enemyY[3]){
			return true;
		}else if((enemyX[4]!=null && enemyY[4]!=null) && x==enemyX[4] && y==enemyY[4]){
			return true;
		}else{
			return false;
		}
	}
	
	
	public void write(FileWriter filewriter){
		try {
			filewriter.write(""+actionCode);
			filewriter.write(","+actionScore);
			
			filewriter.write(","+gameMode);
			filewriter.write(","+currentPage);
			filewriter.write(","+currentPagePosiX);
			filewriter.write(","+detailPlayerX);
			filewriter.write(","+detailPlayerXPage);
			
			filewriter.write(","+playerX);
			filewriter.write(","+playerY);
			filewriter.write(","+marioState);
			filewriter.write(","+marioDieFlg);
			filewriter.write(","+marioScore);
			filewriter.write(","+marioXSpeed);
			
			for(int i=0;i<5;i++){
				filewriter.write(","+enemyX[i]);
			}
			for(int i=0;i<5;i++){
				filewriter.write(","+enemyY[i]);
			}
			for(int i=0;i<TILE_PAGE_WIDTH_NUM*2;i++){
				for(int j=0;j<TILE_HEIGHT_NUM;j++){
					filewriter.write(","+pageBlock[i][j]);
				}
			}
			filewriter.write("\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void read(String dataLine){
		String[] strSplit = dataLine.split(",");
		actionCode = Integer.parseInt(strSplit[0]);
		actionScore = Double.parseDouble(strSplit[1]);
		
		gameMode = Integer.parseInt(strSplit[2]);
		currentPage = Integer.parseInt(strSplit[3]);
		currentPagePosiX = Integer.parseInt(strSplit[4]);
		detailPlayerX = Integer.parseInt(strSplit[5]);
		detailPlayerXPage = Integer.parseInt(strSplit[6]);
		
		playerX = Integer.parseInt(strSplit[7]);
		playerY = Integer.parseInt(strSplit[8]);
		marioState = Integer.parseInt(strSplit[9]);
		marioDieFlg = Integer.parseInt(strSplit[10]);
		marioScore = Integer.parseInt(strSplit[11]);
		marioXSpeed = Integer.parseInt(strSplit[12]);
		
		for(int i=0;i<5;i++){
			if(strSplit[13+i].equals("null")){
				enemyX[i] = null;
			}else{
				enemyX[i] = Integer.parseInt(strSplit[13+i]);
			}
		}
		for(int i=0;i<5;i++){
			if(strSplit[18+i].equals("null")){
				enemyY[i] = null;
			}else{
				enemyY[i] = Integer.parseInt(strSplit[18+i]);
			}
		}
		for(int i=0;i<TILE_PAGE_WIDTH_NUM*2;i++){
			for(int j=0;j<TILE_HEIGHT_NUM;j++){
				pageBlock[i][j] = Boolean.parseBoolean(strSplit[23+j+i*TILE_HEIGHT_NUM]);
			}
		}
	}
}
