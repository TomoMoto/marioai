package tomomoto;

import jp.tanakh.bjne.nes.Nes;

public class MarioDataReader {

	//private int count =0;
	//private static final int ACTION_TERM_COUNT =100;
	public static final int SCREEN_WIDTH = 256;
	public static final int SCREEN_HEIGHT = 240;
	
	public static final int TILE_WIDTH_NUM_PAGE = SCREEN_WIDTH/16;
	private static final int TILE_HEIGHT_NUM = (SCREEN_HEIGHT/16)-2;
	
	//RAM番地
	
	//ゲームモード判定(0:demo,1:normal,3:GameOver)
	private static final short RAM_GAME_MODE = 0x0770;
	//マリオ状態(0:Small,1:big,over2:fiery)
	private static final short RAM_MARIO_STATE = 0x0756;
	//死亡フラグ
	private static final short RAM_MARIO_DIE_FLG = 0x07B1;
	//MARIO_Xの加速度
	private static final short RAM_MARIO_X_SPEED = 0x0057;
	//現在のWORLD
	private static final short RAM_WORLD = 0x075F;
	//現在のLEVEL
	private static final short RAM_LEVEL = 0x0760;
	//MarioScore
	private static final short RAM_MARIO_SCORE_END = 0x07DD;
	//ブロックの位置情報を格納したメモリのスタート番地
	private static final short RAM_TILE_HITBOX_START = 0x0500;
	//左端のページ情報
	private static final short RAM_MAP_CURRENT_PAGE = 0x071A;
	//左端のページのX座標
	private static final short RAM_MAP_CURRENT_PAGE_X = 0x071C;
	//マリオがいるページ内のX座標情報
	private static final short RAM_PLAYER_SCREEN_X = 0x0086;
	//マリオがいるページ番号
	private static final short RAM_PLAYER_SCREEN_PAGE = 0x006D;
	//マリオがいるY座標情報
	private static final short RAM_PLAYER_SCREEN_Y = 0x00CE;
	//敵がいるページ内のX座標情報を格納したメモリのスタート番地
	private static final short RAM_ENEMY_SCREEN_X_START = 0x0087;
	//敵がいるページ番号を格納したメモリのスタート番地
	private static final short RAM_ENEMY_SCREEN_PAGE_START = 0x006E;
	//敵がいるY座標情報を格納したメモリのスタート番地
	private static final short RAM_ENEMY_SCREEN_Y_START = 0x00CF;
	
	/*
	public void printBGR(byte[] bgr){
		count++;
		if(count > ACTION_TERM_COUNT){
			count = 0;
			for (int i=0; i<SCREEN_HEIGHT; i++) {
				for(int j=0; j<SCREEN_WIDTH; j++){
					System.out.println(
							(bgr[(SCREEN_WIDTH*i+j)*3] & 0xff)+","
							+(bgr[(SCREEN_WIDTH*i+j)*3+1] & 0xff)+","
							+(bgr[(SCREEN_WIDTH*i+j)*3+2] & 0xff)+"\n");
				}
			}
			System.out.println("\n");
		}
	}
	
	public void convert2BW(byte[] bgr){
		long tmpBW = 0l;
		count++;
		
		if(count > ACTION_TERM_COUNT){
			count = 0;
			for (int i=0; i<SCREEN_HEIGHT; i++) {
				for(int j=0; j<SCREEN_WIDTH; j++){
					tmpBW =(long)((bgr[(SCREEN_WIDTH*i+j)*3] & 0xff)
						      +(bgr[(SCREEN_WIDTH*i+j)*3+1] & 0xff)
						      +(bgr[(SCREEN_WIDTH*i+j)*3+2] & 0xff))/3;
					bgr[(SCREEN_WIDTH*i+j)*3+0] = (byte) (tmpBW);
					bgr[(SCREEN_WIDTH*i+j)*3+1] = (byte) (tmpBW);
					bgr[(SCREEN_WIDTH*i+j)*3+2] = (byte) (tmpBW);
				}
			}
		}
	}
	*/
	
	public DataSnapMemory readMemory(Nes nes){
		DataSnapMemory snapMemoryData = new DataSnapMemory();
		
		setGameMode(nes, snapMemoryData);
		setMarioData(nes, snapMemoryData);
		setMarioScore(nes, snapMemoryData);
		setStageData(nes, snapMemoryData);
		setCurrentPageData(nes, snapMemoryData);
		setBlockData(nes, snapMemoryData);
		setPlayerData(nes, snapMemoryData);
		setEnemyData(nes, snapMemoryData);
		
		return snapMemoryData;
	}
	
	/**
	 * ゲームモードの取得
	 * @param nes
	 * @param snapMemoryData
	 */
	private void setGameMode(Nes nes, DataSnapMemory snapMemoryData){
		int gameMode = (nes.getMbc().read(RAM_GAME_MODE) & 0xff);
		snapMemoryData.setGameMode(gameMode);
	}
	
	/**
	 * マリオの状態取得
	 * @param nes
	 * @param snapMemoryData
	 */
	private void setMarioData(Nes nes, DataSnapMemory snapMemoryData){
		int marioState = (nes.getMbc().read(RAM_MARIO_STATE) & 0xff);
		int marioDieFlg = (nes.getMbc().read(RAM_MARIO_DIE_FLG) & 0xff);
		snapMemoryData.setMarioState(marioState);
		snapMemoryData.setMarioDieFlg(marioDieFlg);
		int marioXSpeed = (nes.getMbc().read(RAM_MARIO_X_SPEED) & 0xff);
		snapMemoryData.setMarioXSpeed(marioXSpeed);
	}
	
	/**
	 * マリオのScore取得
	 * @param nes
	 * @param snapMemoryData
	 */
	private void setMarioScore(Nes nes, DataSnapMemory snapMemoryData){
		int marioScore = 0;
		for(short i=0;i<6;i++){
			marioScore = marioScore * 10;
			marioScore = marioScore + (nes.getMbc().read((short)(RAM_MARIO_SCORE_END+i)) & 0xff);
		}
		snapMemoryData.setMarioScore(marioScore);
	}
	
	/**
	 * ステージの状態取得
	 * @param nes
	 * @param snapMemoryData
	 */
	private void setStageData(Nes nes, DataSnapMemory snapMemoryData){
		int stageWorld = (nes.getMbc().read(RAM_WORLD) & 0xff);
		int stageLevel = (nes.getMbc().read(RAM_LEVEL) & 0xff);
		snapMemoryData.setStageWorld(stageWorld);
		snapMemoryData.setStageLevel(stageLevel);
	}
	
	/**
	 * 現在の左端の位置情報
	 * @param nes
	 * @param snapMemoryData
	 */
	private void setCurrentPageData(Nes nes, DataSnapMemory snapMemoryData){
		int currentPage = (nes.getMbc().read(RAM_MAP_CURRENT_PAGE) & 0xff);
		int currentPageX = (nes.getMbc().read(RAM_MAP_CURRENT_PAGE_X) & 0xff);
		snapMemoryData.setCurrentPage(currentPage);
		snapMemoryData.setCurrentPagePosiX(currentPageX);// /16
	}
	
	/**
	 * ブロックの位置情報取得
	 * @param nes
	 * @param snapMemoryData
	 */
	private void setBlockData(Nes nes, DataSnapMemory snapMemoryData){
		Boolean[][] pageBlock = snapMemoryData.getPageBlock();
		
		short tmpRamNum = RAM_TILE_HITBOX_START;
		for (short i=0; i<TILE_HEIGHT_NUM; i++) {
			for(short j=0; j<TILE_WIDTH_NUM_PAGE; j++){
				//左端のスクロール判定（ページ偶数）
				if(snapMemoryData.getCurrentPage()%2==0 &&
						(j==snapMemoryData.getCurrentPagePosiX()/16-1 || j==snapMemoryData.getCurrentPagePosiX()/16-2)){
					pageBlock[j][i]=true;
				}else{
					int tileData = (nes.getMbc().read((short) (tmpRamNum + TILE_WIDTH_NUM_PAGE*i +j)) & 0xff);
					if(tileData>0){
						pageBlock[j][i]=true;
					}else{
						pageBlock[j][i]=false;					
					}
				}
			}
			for(short j=0; j<TILE_WIDTH_NUM_PAGE; j++){
				//左端のスクロール判定（ページ偶数）
				if(snapMemoryData.getCurrentPage()%2==1 &&
						(j==snapMemoryData.getCurrentPagePosiX()/16-1 || j==snapMemoryData.getCurrentPagePosiX()/16-2)){
					pageBlock[TILE_WIDTH_NUM_PAGE+j][i]=true;
				}else{
					int tileData = (nes.getMbc().read((short) (tmpRamNum +TILE_HEIGHT_NUM*TILE_WIDTH_NUM_PAGE +TILE_WIDTH_NUM_PAGE*i +j)) & 0xff);
					if(tileData>0){
						pageBlock[TILE_WIDTH_NUM_PAGE+j][i]=true;
					}else{
						pageBlock[TILE_WIDTH_NUM_PAGE+j][i]=false;					
					}
				}
			}
		}
		snapMemoryData.setPageBlock(pageBlock);
	}
	
	/**
	 * マリオの位置情報取得
	 * @param nes
	 * @param snapMemoryData
	 */
	private void setPlayerData(Nes nes, DataSnapMemory snapMemoryData){
		int playerX = ((nes.getMbc().read(RAM_PLAYER_SCREEN_X) & 0xff));// /16
		int playerXPage = ((nes.getMbc().read(RAM_PLAYER_SCREEN_PAGE) & 0xff));
		snapMemoryData.setDetailPlayerX(playerX);
		snapMemoryData.setDetailPlayerXPage(playerXPage);
		
		if(playerXPage%2==1){
			playerX = playerX + TILE_WIDTH_NUM_PAGE;//)%(TILE_WIDTH_NUM_PAGE*2);
		}
		int playerY = ((nes.getMbc().read(RAM_PLAYER_SCREEN_Y) & 0xff)-16);// /16
		
		snapMemoryData.setPlayerX(playerX);
		snapMemoryData.setPlayerY(playerY);
		
		
	}
	
	/**
	 * 敵の位置情報取得
	 * @param nes
	 * @param snapMemoryData
	 */
	private void setEnemyData(Nes nes, DataSnapMemory snapMemoryData){
		Integer[] enemyX = snapMemoryData.getEnemyX();
		Integer[] enemyY = snapMemoryData.getEnemyY();
		
		
		short tmpEnemyX = RAM_ENEMY_SCREEN_X_START;
		short tmpEnemyY = RAM_ENEMY_SCREEN_Y_START;
		short tmpEnemyPage = RAM_ENEMY_SCREEN_PAGE_START;
		
		for(short i=0;i<5;i++){
			enemyX[i] = ((nes.getMbc().read(tmpEnemyX) & 0xff));
			enemyY[i] = ((nes.getMbc().read(tmpEnemyY) & 0xff));
			
			if(enemyX[i]==0 || enemyY[i]==0){
				enemyX[i]=null;
				enemyY[i]=null;
			}else{
				//enemyX[i] = enemyX[i]/16;
				int playerXPage = ((nes.getMbc().read((short) (tmpEnemyPage)) & 0xff));
				if(playerXPage%2==1){
					enemyX[i] = (enemyX[i] + TILE_WIDTH_NUM_PAGE)%(TILE_WIDTH_NUM_PAGE*2);
				}
				enemyY[i] = enemyY[i]-16; // /16
			}
			
			tmpEnemyX++;
			tmpEnemyY++;
			tmpEnemyPage++;
		}
		
		snapMemoryData.setEnemyX(enemyX);
		snapMemoryData.setEnemyY(enemyY);
	}
	
}
