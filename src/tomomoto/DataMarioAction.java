package tomomoto;

public class DataMarioAction {
	private int time = 1;
	private boolean jumpFlg = false;
	private boolean dashFlg = false;
	private boolean leftFlg = false;
	private boolean rightFlg = false;
	private boolean downFlg = false;
	private boolean startFlg = false;

	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public boolean isJumpFlg() {
		return jumpFlg;
	}
	public void setJumpFlg(boolean jumpFlg) {
		this.jumpFlg = jumpFlg;
	}
	public boolean isDashFlg() {
		return dashFlg;
	}
	public void setDashFlg(boolean dashFlg) {
		this.dashFlg = dashFlg;
	}
	public boolean isLeftFlg() {
		return leftFlg;
	}
	public void setLeftFlg(boolean leftFlg) {
		this.leftFlg = leftFlg;
	}
	public boolean isRightFlg() {
		return rightFlg;
	}
	public void setRightFlg(boolean rightFlg) {
		this.rightFlg = rightFlg;
	}
	public boolean isDownFlg() {
		return downFlg;
	}
	public void setDownFlg(boolean downFlg) {
		this.downFlg = downFlg;
	}
	public boolean isStartFlg() {
		return startFlg;
	}
	public void setStartFlg(boolean startFlg) {
		this.startFlg = startFlg;
	}
	
	public boolean isEnd(){
		time--;
		if(time <= 0){
			return true;
		}
		return false;
	}
	
}
