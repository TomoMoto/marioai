package tree;

public class TreeData {
	private boolean answerFlg = false;
	private double[] paraDataSet;
	
	public boolean isAnswerFlg() {
		return answerFlg;
	}
	public void setAnswerFlg(boolean answerFlg) {
		this.answerFlg = answerFlg;
	}
	public double[] getParaDataSet() {
		return paraDataSet;
	}
	public void setParaDataSet(double[] paraDataSet) {
		this.paraDataSet = paraDataSet;
	}
	
}
