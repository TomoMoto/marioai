package tree;

public class TreeAnswer {
	private double trueCnt = 0d;
	private double falseCnt = 0d;
	
	public double getTrueCnt() {
		return trueCnt;
	}
	public void setTrueCnt(double trueCnt) {
		this.trueCnt = trueCnt;
	}
	public double getFalseCnt() {
		return falseCnt;
	}
	public void setFalseCnt(double falseCnt) {
		this.falseCnt = falseCnt;
	}
	
}
