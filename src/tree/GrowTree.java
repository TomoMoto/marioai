package tree;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GrowTree {

	private Integer targetParaNum = null;
	private double targetParaValue = 0d;
	
	private GrowTree leftTree;
	private GrowTree rightTree;
	
	private int depth;
	private double trueCnt = 0;
	private double falseCnt = 0;	
	
	public GrowTree clone(){
		GrowTree clone = new GrowTree();
		clone.setTargetParaNum(targetParaNum);
		clone.setTargetParaValue(targetParaValue);
		clone.setDepth(depth);
		clone.setTrueCnt(trueCnt);
		clone.setFalseCnt(falseCnt);
		if(rightTree == null){
			clone.setRightTree(null);
		}else{
			clone.setRightTree(rightTree.clone());
		}
		if(leftTree == null){
			clone.setLeftTree(null);
		}else{
			clone.setLeftTree(leftTree.clone());
		}
		
		return clone;
	}

	public TreeAnswer apply(TreeData treeData){
		if(targetParaNum == null){
			TreeAnswer treeAnswer = new TreeAnswer();
			treeAnswer.setTrueCnt(trueCnt);
			treeAnswer.setFalseCnt(falseCnt);
			return treeAnswer;
		}else{
			if(treeData.getParaDataSet()[targetParaNum] >= targetParaValue){
				return rightTree.apply(treeData);
			}else{
				return leftTree.apply(treeData);
			}
		}
	}
	
	public void study(List<TreeData> deadTreeDataList, List<TreeData> goodMemoryList){
		study(deadTreeDataList, goodMemoryList, 1);
	}
	
	private void study(List<TreeData> deadTreeDataList, List<TreeData> goodMemoryList, int depth){
		if(deadTreeDataList.size()+goodMemoryList.size()<=0){
			return;
		}
		
		if(targetParaNum == null){
			this.depth = depth;
			trueCnt = 0;
			falseCnt = 0;
			for(int i=0;i<deadTreeDataList.size();i++){
				if(deadTreeDataList.get(i).isAnswerFlg()){
					trueCnt++;
				}else{
					falseCnt++;
				}
			}
			for(int i=0;i<goodMemoryList.size();i++){
				if(goodMemoryList.get(i).isAnswerFlg()){
					trueCnt++;
				}else{
					falseCnt++;
				}
			}
			
			if(depth > 200){
				System.out.println("MaxDepth");
				return;
			}else if(trueCnt==0 || falseCnt==0){
				//学習終了
				return;
			}else{
				//決定木深堀
				targetParaNum = null;
				targetParaValue = 0d;
				double bestEn = getEn(trueCnt, falseCnt);
				
				int tmpTargetParaNum;
				double tmpTargetParaValue,tmpTargetEn,tmpTrueCnt1,tmpTrueCnt2,tmpFalseCnt1,tmpFalseCnt2;
				
				int paraLength = 0;
				if(deadTreeDataList.size()>0){
					paraLength = deadTreeDataList.get(0).getParaDataSet().length;
				}else if(goodMemoryList.size()>0){
					paraLength = goodMemoryList.get(0).getParaDataSet().length;
				}
				
				for(int i=0;i<paraLength;i++){
					tmpTargetParaNum = i;
					for(int j=0;j<255*2;j++){
						tmpTargetParaValue = (255d-(double)j);
						tmpTrueCnt1 =0d;
						tmpTrueCnt2 =0d;
						tmpFalseCnt1 =0d;
						tmpFalseCnt2 =0d;
						
						for(int k=0;k<deadTreeDataList.size();k++){
							if(deadTreeDataList.get(k).getParaDataSet()[tmpTargetParaNum] >= tmpTargetParaValue){
								//RightTree
								if(deadTreeDataList.get(k).isAnswerFlg()){
									tmpTrueCnt1++;
								}else{
									tmpFalseCnt1++;
								}
							}else{
								//LeftTree
								if(deadTreeDataList.get(k).isAnswerFlg()){
									tmpTrueCnt2++;
								}else{
									tmpFalseCnt2++;
								}
							}
						}
						for(int k=0;k<goodMemoryList.size();k++){
							if(goodMemoryList.get(k).getParaDataSet()[tmpTargetParaNum] >= tmpTargetParaValue){
								//RightTree
								if(goodMemoryList.get(k).isAnswerFlg()){
									tmpTrueCnt1++;
								}else{
									tmpFalseCnt1++;
								}
							}else{
								//LeftTree
								if(goodMemoryList.get(k).isAnswerFlg()){
									tmpTrueCnt2++;
								}else{
									tmpFalseCnt2++;
								}
							}
						}
						tmpTargetEn = getEn(tmpTrueCnt1, tmpFalseCnt1, tmpTrueCnt2, tmpFalseCnt2);
						
						if(tmpTargetEn<bestEn){
							bestEn = tmpTargetEn;
							targetParaNum = tmpTargetParaNum;
							targetParaValue = tmpTargetParaValue;
						}
					}
				}
				
				
				if(targetParaNum != null){
					List<TreeData> rightDeadTreeData = new ArrayList<TreeData>();
					List<TreeData> leftDeadTreeData = new ArrayList<TreeData>();
					
					for(int k=0;k<deadTreeDataList.size();k++){
						if(deadTreeDataList.get(k).getParaDataSet()[targetParaNum] >= targetParaValue){
							//RightTree
							rightDeadTreeData.add(deadTreeDataList.get(k));
						}else{
							//LeftTree
							leftDeadTreeData.add(deadTreeDataList.get(k));
						}
					}
					
					List<TreeData> rightGoodTreeData = new ArrayList<TreeData>();
					List<TreeData> leftGoodTreeData = new ArrayList<TreeData>();
					
					for(int k=0;k<goodMemoryList.size();k++){
						if(goodMemoryList.get(k).getParaDataSet()[targetParaNum] >= targetParaValue){
							//RightTree
							rightGoodTreeData.add(goodMemoryList.get(k));
						}else{
							//LeftTree
							leftGoodTreeData.add(goodMemoryList.get(k));
						}
					}
					rightTree = new GrowTree();
					leftTree = new GrowTree();
					rightTree.study(rightDeadTreeData,rightGoodTreeData,depth+1);
					leftTree.study(leftDeadTreeData,leftGoodTreeData,depth+1);
				}
			}
		}else{
			//壊さないで適用
			List<TreeData> rightDeadTreeData = new ArrayList<TreeData>();
			List<TreeData> leftDeadTreeData = new ArrayList<TreeData>();
			
			for(int k=0;k<deadTreeDataList.size();k++){
				if(deadTreeDataList.get(k).getParaDataSet()[targetParaNum] >= targetParaValue){
					//RightTree
					rightDeadTreeData.add(deadTreeDataList.get(k));
				}else{
					//LeftTree
					leftDeadTreeData.add(deadTreeDataList.get(k));
				}
			}
			
			List<TreeData> rightGoodTreeData = new ArrayList<TreeData>();
			List<TreeData> leftGoodTreeData = new ArrayList<TreeData>();
			
			for(int k=0;k<goodMemoryList.size();k++){
				if(goodMemoryList.get(k).getParaDataSet()[targetParaNum] >= targetParaValue){
					//RightTree
					rightGoodTreeData.add(goodMemoryList.get(k));
				}else{
					//LeftTree
					leftGoodTreeData.add(goodMemoryList.get(k));
				}
			}
			rightTree.study(rightDeadTreeData,rightGoodTreeData,depth+1);
			leftTree.study(leftDeadTreeData,leftGoodTreeData,depth+1);
		}
		
	}
	
	public void printModel(){
		for(int i=0;i<depth-1;i++){
			System.out.print("-");
		}
		System.out.println(targetParaNum+"/"+targetParaValue+":"+trueCnt+"/"+falseCnt);
		if(rightTree != null){
			rightTree.printModel();
		}
		if(leftTree != null){
			leftTree.printModel();
		}
	}
	
	private double getEn(double trueCnt, double falseCnt){
		if(trueCnt+falseCnt==0){
			return 1000000;
		}
		return (trueCnt/(trueCnt+falseCnt)) * Math.log(1-(trueCnt/(trueCnt+falseCnt)));
	}
	
	private double getEn(double trueCnt1, double falseCnt1,double trueCnt2, double falseCnt2){
		return (trueCnt1+falseCnt1)/(trueCnt1+falseCnt1+trueCnt2+falseCnt2)
		* (trueCnt1/(trueCnt1+falseCnt1)) * Math.log(1-(trueCnt1/(trueCnt1+falseCnt1)))
		+(trueCnt2+falseCnt2)/(trueCnt1+falseCnt1+trueCnt2+falseCnt2)
		* (trueCnt2/(trueCnt2+falseCnt2)) * Math.log(1-(trueCnt2/(trueCnt2+falseCnt2)));
	}
	
	public void write(FileWriter filewriter){
		try {
			filewriter.write(targetParaNum+"");
			filewriter.write(","+targetParaValue);
			filewriter.write(","+depth);
			filewriter.write(","+trueCnt);
			filewriter.write(","+falseCnt);
			if(leftTree==null){
				filewriter.write(","+"noLeftTree");
			}else{
				filewriter.write(","+"leftTree"+",");
				leftTree.write(filewriter);
			}
			if(rightTree==null){
				filewriter.write(","+"noRightTree");
			}else{
				filewriter.write(","+"rightTree"+",");
				rightTree.write(filewriter);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public GrowTree read(String dataLine){
		String[] dataLines = dataLine.split(",");
		GrowTree tree = new GrowTree();
		read(tree, dataLines, 0);
		return tree;
	}
	
	public int read(GrowTree tree, String[] dataLines, Integer startNum){
		if("null".equals(dataLines[startNum])){
			tree.setTargetParaNum(null);
		}else{
			tree.setTargetParaNum(Integer.parseInt(dataLines[startNum]));
		}
		startNum++;
		
		tree.setTargetParaValue(Double.parseDouble(dataLines[startNum]));
		startNum++;
		
		tree.setDepth(Integer.parseInt(dataLines[startNum]));
		startNum++;
		
		tree.setTrueCnt(Double.parseDouble(dataLines[startNum]));
		startNum++;
		
		tree.setFalseCnt(Double.parseDouble(dataLines[startNum]));
		startNum++;
		
		if("noLeftTree".equals(dataLines[startNum])){
			tree.setLeftTree(null);
			startNum++;
		}else{
			startNum++;
			GrowTree leftTree = new GrowTree();
			startNum = read(leftTree, dataLines, startNum);
			tree.setLeftTree(leftTree);
		}
		
		if("noRightTree".equals(dataLines[startNum])){
			tree.setRightTree(null);
			startNum++;
		}else{
			startNum++;
			GrowTree rightTree = new GrowTree();
			startNum = read(rightTree, dataLines, startNum);
			tree.setRightTree(rightTree);
		}
		return startNum;
	}
	
	
	public void setTargetParaNum(Integer targetParaNum) {
		this.targetParaNum = targetParaNum;
	}
	public void setTargetParaValue(double targetParaValue) {
		this.targetParaValue = targetParaValue;
	}
	public void setLeftTree(GrowTree leftTree) {
		this.leftTree = leftTree;
	}
	public void setRightTree(GrowTree rightTree) {
		this.rightTree = rightTree;
	}
	public void setTrueCnt(double trueCnt) {
		this.trueCnt = trueCnt;
	}
	public void setFalseCnt(double falseCnt) {
		this.falseCnt = falseCnt;
	}
	public void setDepth(int depth) {
		this.depth = depth;
	}

}
